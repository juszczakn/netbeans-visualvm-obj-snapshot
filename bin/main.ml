open Core
open Visualvm_parser

let package_col, count_col = "Name", "Count"

let filenames = ["test1.csv"; "test2.csv"]

let () =
  let read_file_exn = Perfile.read_file_exn package_col count_col int_of_string in
  let perfiles = List.map filenames ~f:(fun file -> read_file_exn file) in
  let aggr_perfile = Aggregate_perfile.aggregate perfiles in
  let pred lst =
    let int_cmp = fun a b -> if a > b then -1 else if a < b then 1 else 0 in
    let sorted_list = List.sort ~cmp:int_cmp lst in
    List.equal lst sorted_list ~equal:(fun a b -> a = b)
  in
  let csv = Aggregate_perfile.to_csv ~predicate:pred aggr_perfile in
  Csv.print_readable csv
