open Core

type t = int String.Table.t

let read_file_exn classname_col_name count_col_name to_int filename =
  let csv = Csv.load filename in
  let header, data = match csv with
    | h :: d -> h, d
    | [] -> assert false
  in
  let elts = Csv.associate header data in
  let col_to_row_data = List.map elts ~f:(fun a -> (Map.of_alist_exn (module String) a)) in
  let create_name_to_count_tbl rows =
    let tbl = String.Table.create () in
    let insert m =
      let class_name = Map.find_exn m classname_col_name in
      let count = Map.find_exn m count_col_name in
      Hashtbl.add_exn tbl ~key:class_name ~data:(to_int count)
    in
    List.iter rows ~f:insert;
    tbl
  in
  create_name_to_count_tbl col_to_row_data

let keys perfile =
  Hashtbl.keys perfile 

let get_count perfile key =
  Hashtbl.find perfile key

let to_csv p =
  let all_keys = Hashtbl.keys p in
  let mapper k =
    let v = Hashtbl.find_exn p k in
    [|k; (string_of_int v)|]
  in
  let all_rows = Array.of_list (List.map all_keys ~f:mapper) in
  Csv.of_array all_rows
