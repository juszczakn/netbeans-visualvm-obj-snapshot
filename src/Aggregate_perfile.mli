type t
(** int list String.Table.t *)

val aggregate : Perfile.t list -> t
(** Aggregate a list of Perfiles into a collection which can in turn
    be processed and turned into a csv. *)

val to_csv : ?predicate:(int list -> bool) -> t -> Csv.t
(** convert t -> csv, keeping only kv's whose values pass predicate.
    predicate defaults to keeping everything *)
