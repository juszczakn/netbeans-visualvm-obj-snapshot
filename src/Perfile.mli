type t
(** A single file's data *)

val read_file_exn : string -> string -> (string -> int) -> string -> t
(** classname_col_name count_col_name filename *)

val keys : t -> string list
(** get all classnames *)

val get_count : t -> string -> int option
(** Perfile -> classname -> count option *)

val to_csv : t -> Csv.t
(** Perfile -> Csv *)
