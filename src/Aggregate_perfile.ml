open Core

type t = int list String.Table.t

let get_all_keys perfiles =
  let keys = List.map ~f:Perfile.keys perfiles in
  Set.of_list (module String) (List.concat keys)

let aggregate perfiles =
  let add tbl k v =
    let value = match Hashtbl.find tbl k with
      |Some elts -> v :: elts
      |None -> [v]
    in
    Hashtbl.set tbl ~key:k ~data:value
  in
  let putall tbl key_superset p =
    Set.iter key_superset ~f:(
      fun k ->
        let v = match Perfile.get_count p k with
          |Some v -> v
          |None -> 0
        in
        add tbl k v
    ) in
  let all_keys = get_all_keys perfiles in
  let tbl = String.Table.create () in
  List.iter perfiles ~f:(fun p -> putall tbl all_keys p);
  tbl

let keep_everything values = true

let to_csv ?(predicate = keep_everything) aggr =
  let kept_aggr = Hashtbl.filter aggr ~f:predicate in
  let mapper k =
    let lst = Hashtbl.find_exn kept_aggr k in
    let str_lst = List.map lst ~f:string_of_int in
    Array.of_list (k :: (List.rev str_lst))
  in
  let all_keys = Hashtbl.keys kept_aggr in
  let all_rows = Array.of_list (List.map all_keys ~f:mapper) in
  Csv.of_array all_rows
