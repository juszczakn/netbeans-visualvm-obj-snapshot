* VisualVM Parser

** About

This is mostly a learning excercise, but the main idea is to be able to easily parse
Netbeans/VisualVM object count memory snapshots of a running JVM, process the results
and output a csv of compiled data.

See bin/main.ml for an example.

** Build

To build and run the example:

#+BEGIN_SRC bash
jbuilder build bin/main.exe
./_build/default/bin/main.exe 
#+END_SRC

